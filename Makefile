.POSIX:

PREFIX = /usr/local
CC = gcc
HAXE = haxe

TOOLS := packtextures extractatlas insertatlas jsontoatlas

all: $(TOOLS)

%: %_hlc/main.c
	$(LINK.c) -I$*_hlc $^ $(LOADLIBES) $(LDLIBS) -lhl -l:fmt.hdll -o $@

define build_hl
	$(HAXE) \
		--class-path src \
		--library format \
		--library bin-packing \
		--main $(<F) \
		--hl $@
endef

packtextures_hlc/main.c: src/TexturePacker.hx; $(build_hl)
packtextures.hl: src/TexturePacker.hx; $(build_hl)
extractatlas_hlc/main.c: src/AtlasExtractor.hx; $(build_hl)
extractatlas.hl: src/AtlasExtractor.hx; $(build_hl)
insertatlas_hlc/main.c: src/AtlasInserter.hx; $(build_hl)
insertatlas.hl: src/AtlasInserter.hx; $(build_hl)
jsontoatlas_hlc/main.c: src/JsonToAtlas.hx; $(build_hl)
jsontoatlas.hl: src/JsonToAtlas.hx; $(build_hl)

install: all
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	for tool in $(TOOLS); do \
		cp -f $$tool $(DESTDIR)$(PREFIX)/bin; \
		chmod 755 $(DESTDIR)$(PREFIX)/bin/$$tool; \
	done

uninstall:
	for tool in $(TOOLS); do \
		rm -f $(DESTDIR)$(PREFIX)/bin/$$tool; \
	done

clean:
	rm -f $(TOOLS) $(patsubst %,%.hl,$(TOOLS))
	rm -fr $(patsubst %,%_hlc,$(TOOLS))

.PHONY: all clean install uninstall
