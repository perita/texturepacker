import sys.io.File;
import binpacking.MaxRectsPacker;
import haxe.crypto.Sha1;

class TexturePacker {
	public static function main() {
		final args = Sys.args();
		if (args.length < 2) {
			showUsage();
		}
		final files = args.slice(0, -1);
		final output = args[args.length - 1];

		final indexed_key = ~/^(.+)_(\d+)$/;

		final tileMap = new Map<String, Tile>();
		final contents = new Map<String, Array<Tile>>();
		for (file in files) {
			final input = File.read(file, true);
			final image = new format.png.Reader(input).read();
			final header = format.png.Tools.getHeader(image);
			final data = format.png.Tools.extract32(image);
			final hash = Sha1.make(data).toHex();
			var tile = tileMap.get(hash);
			if (tile == null) {
				tile = new Tile(header.width, header.height, data);
				tileMap.set(hash, tile);
			}

			var key = haxe.io.Path.withoutExtension(haxe.io.Path.withoutDirectory(file));
			var index = 0;
			if (indexed_key.match(key)) {
				key = indexed_key.matched(1);
				index = Std.parseInt(indexed_key.matched(2));
			}
			var entry = contents.get(key);
			if (entry == null) {
				entry = [];
				contents.set(key, entry);
			}
			entry[index] = tile;
		}

		final tiles = [for (tile in tileMap.iterator()) tile];
		final result = findBest(tiles);
		if (result == null) {
			Sys.stderr().writeString("Could not pack images.\n");
			Sys.exit(-1);
		}

		final packer = new MaxRectsPacker(result.w, result.h, false);
		final atlas = new Atlas(result.w, result.h, contents);
		for (tile in tiles) {
			final rect = packer.insert(tile.w + padding_x, tile.h + padding_y, result.heuristic);
			tile.x = Std.int(rect.x + padding_x / 2);
			tile.y = Std.int(rect.y + padding_y / 2);
			atlas.blit(tile);
		}
		File.saveBytes(output, atlas.toPNG());
	}

	public static function showUsage() {
		Sys.stderr().writeString("Usage: packtextures input0.png input2.png... output.png\n");
		Sys.exit(-1);
	}

	static function findBest(tiles: Array<Tile>): Result {
		var best = null;
		for (heuristic in Type.allEnums(FreeRectChoiceHeuristic)) {
			for(width in 9...12) {
				final binWidth = Std.int(Math.pow(2, width));
				for(height in 9...12) {
					final binHeight = Std.int(Math.pow(2, height));
					final packer = new MaxRectsPacker(binWidth, binHeight, false);
					var failed = false;
					for(tile in tiles) {
						final rect = packer.insert(tile.w + padding_x, tile.h + padding_y, heuristic);
						if (rect == null || rect.x + tile.w > binWidth || rect.y + tile.h > binHeight) {
							failed = true;
							break;
						}
					}
					if (failed) {
						continue;
					}
					final occupancy = packer.occupancy();
					if (best == null || best.occupancy < occupancy) {
						best = new Result(binWidth, binHeight, heuristic, occupancy);
					}
				}
			}
		}
		return best;
	}

	static final padding_x = 2;
	static final padding_y = 2;
}

class Tile {
	public var x: Int;
	public var y: Int;
	public var w: Int;
	public var h: Int;
	public var dx: Int;
	public var dy: Int;
	public var origW: Int;
	public var origH: Int;
	public final bytes: haxe.io.Bytes;
	public final stride : Int;
	private final bytesPerPixel = 4;

	public function new(width: Int, height: Int, bytes: haxe.io.Bytes) {
		w = width;
		h = height;
		dx = 0;
		dy = 0;
		origW = width;
		origH = height;
		this.bytes = bytes;
		stride = width * bytesPerPixel;
	}

	public function toString() {
		return 'Tile($x, $y, $w, $h)';
	}
}

class Result {
	public var w: Int;
	public var h: Int;
	public var heuristic: FreeRectChoiceHeuristic;
	public var occupancy: Float;

	public function new(w: Int, h: Int, heuristic: FreeRectChoiceHeuristic, occupancy: Float) {
		this.w = w;
		this.h = h;
		this.heuristic = heuristic;
		this.occupancy = occupancy;
	}

	public function toString() {
		return 'Result($w, $h, $heuristic, $occupancy)';
	}
}

class Atlas {
	private final width: Int;
	private final height: Int;
	private final bytes : haxe.io.Bytes;
	private final stride : Int;
	private final bytesPerPixel = 4;
	private final contents: Map<String, Array<Tile>>;

	public function new(width : Int, height : Int, contents: Map<String, Array<Tile>>) {
		this.width = width;
		this.height = height;
		stride = width * bytesPerPixel;
		this.bytes = haxe.io.Bytes.alloc(height * stride);
		this.contents = contents;
	}

	public function blit(tile: Tile) {
		var inP = 0;
		final x = tile.x;
		final y = tile.y;
		var outP = y * stride + x * bytesPerPixel;
		for( dy in 0...tile.h ) {
			bytes.blit(outP, tile.bytes, inP, tile.stride);
			inP += tile.stride;
			outP += stride;
		}
	}

	public function toPNG( ?level = 9 ) {
		var png;
		png = std.format.png.Tools.build32BGRA(width, height, bytes #if (format >= "3.3") , level #end);
		final end = png.last();
		png.remove(end);
		final txt = new haxe.io.BytesOutput();
		txt.writeString('Atlas');
		txt.writeByte(0);
		txt.writeByte(0);
		txt.write(std.format.tools.Deflate.run(getContents(), level));
		png.add(std.format.png.Data.Chunk.CUnknown('zTXt', txt.getBytes()));
		png.add(end);
		var o = new haxe.io.BytesOutput();
		new format.png.Writer(o).write(png);
		return o.getBytes();
	}

	private function getContents(): haxe.io.Bytes {
		final txt = new haxe.io.BytesOutput();
		for (key => tiles in contents) {
			for(index => tile in tiles) {
				if (tile == null) continue;
				txt.writeString('$key\n');
				txt.writeString(' rotate: false\n');
				txt.writeString(' xy: ${tile.x}, ${tile.y}\n');
				txt.writeString(' size: ${tile.w}, ${tile.h}\n');
				txt.writeString(' orig: ${tile.origW}, ${tile.origH}\n');
				txt.writeString(' offset: ${tile.dx}, ${tile.dy}\n');
				txt.writeString(' index: ${tiles.length == 1 ? -1 : index}\n');
			}
		}
		return txt.getBytes();
	}

	public function toString() {
		return 'Atlas($width, $height)';
	}
}
