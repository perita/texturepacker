import sys.io.File;

class AtlasExtractor {
	public static function main() {
		final args = Sys.args();
		if (args.length < 2) {
			showUsage();
		}
		final files = args.slice(0, -1);
		final output = args[args.length - 1];

		final result = new haxe.io.BytesOutput();
		for(file in files) {
			var input;
			try {
				input = File.read(file, true);
			} catch (e) {
				Sys.stderr().writeString('Warning: $file not found\n');
				continue;
			}
			var image;
			try{
				image = new format.png.Reader(input).read();
			} catch (e) {
				Sys.stderr().writeString('Warning: $file is not a PNG file\n');
				continue;
			}
			final header = format.png.Tools.getHeader(image);
			final atlas = getAtlas(image);
			if (atlas == null) {
				Sys.stderr().writeString('Warning: $file does not has an atlas\n');
				continue;
			}
			result.writeString(haxe.io.Path.withoutDirectory(file));
			result.writeByte('\n'.code);
			result.writeString('size: ${header.width}, ${header.height}\n');
			result.writeString('format: RGBA8888\n');
			result.writeString('filter: Nearest, Nearest\n');
			result.writeString('repeat: none\n');
			result.writeString(atlas);
			result.writeByte('\n'.code);
		}
		final bytes = result.getBytes();
		if (bytes.length == 0) {
			Sys.stderr().writeString('Error: No atlas to write.\n');
			Sys.exit(-1);
		}
		if (output == '-') {
			Sys.stdout().write(bytes);
		} else {
			File.saveBytes(output, bytes);
		}
	}

	private static function showUsage() {
		Sys.stderr().writeString("Usage: extractaltas input0.png input2.png... output.atlas\n");
		Sys.exit(-1);
	}

	private static function getAtlas(chunks: format.png.Data): Null<String> {
		for(chunk in chunks) {
			switch(chunk) {
				case CUnknown("zTXt", data):
					final input = new haxe.io.BytesInput(data);
					final keyword = input.readUntil(0);
					if (keyword != 'Atlas') continue;
					final method = input.readByte();
					if (method != 0) continue;
					return format.tools.Inflate.run(input.readAll()).toString();
				default:
			}
		}
		return null;
	}
}
