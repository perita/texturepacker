import sys.io.File;

typedef PosSize = {
	var x: Int;
	var y: Int;
	var w: Int;
	var h: Int;
}

typedef Size = {
	var w: Int;
	var h: Int;
}

typedef Frame = {
	var frame: PosSize;
	var rotated: Bool;
	var trimmed: Bool;
	var spriteSourceSize: PosSize;
	var sourceSize: Size;
	var duration: Int;
}

typedef Meta = {
	var app: String;
	var version: String;
	var image: String;
	var format: String;
	var size: Size;
	var scale: Int;
}

typedef Data = {
	var frames: haxe.DynamicAccess<Frame>;
	var meta: Meta;
}

typedef Atlas = Map<String, Array<Frame>>;

class JsonToAtlas {
	public static function main() {
		final args = Sys.args();
		if (args.length > 0 && (args[0] == '-h' || args[0] == '--help') ) {
			showUsage();
		}

		try {
			final input = args.length > 1 && args[1] != '-' ? File.getContent(args[1]) : Sys.stdin().readAll().toString();
			var data: Data = haxe.Json.parse(input);
			final atlas = new Atlas();
			final indexedKey = ~/^(.+) (\d+)$/;
			for(file in data.frames.keys()) {
				var key = haxe.io.Path.withoutExtension(file);
				var index = 0;
				if (indexedKey.match(key)) {
					key = indexedKey.matched(1);
					index = Std.parseInt(indexedKey.matched(2));
				}
				var tile = atlas.get(key);
				if (tile == null) {
					tile = [];
					atlas.set(key, tile);
				}
				final frame = data.frames[file];
				tile[index] = data.frames[file];
			}
			final atlasContent = buildContent(atlas);
			if (args.length > 0 && args[0] != '-') {
				File.saveBytes(args[0], atlasContent);
			} else {
				Sys.stdout().writeFullBytes(atlasContent, 0, atlasContent.length);
			}
		} catch (e) {
			Sys.stderr().writeString('Could not convert: ${e.message}\n');
			Sys.exit(-1);
		}
	}

	static function showUsage() {
		Sys.stdout().writeString("Usage: json2atlas [output.atlas|- [input.json|-]]\n");
		Sys.exit(0);
	}

	static function buildContent(atlas: Atlas): haxe.io.Bytes {
		final txt = new haxe.io.BytesOutput();
		for (key => tiles in atlas) {
			for(index => tile in tiles) {
				if (tile == null) continue;
				txt.writeString('$key\n');
				txt.writeString(' rotate: ${tile.rotated ? "true" : "false"}\n');
				txt.writeString(' xy: ${tile.frame.x}, ${tile.frame.y}\n');
				txt.writeString(' size: ${tile.frame.w}, ${tile.frame.h}\n');
				txt.writeString(' orig: ${tile.sourceSize.w}, ${tile.sourceSize.h}\n');
				txt.writeString(' offset: ${tile.spriteSourceSize.x}, ${tile.spriteSourceSize.y}\n');
				txt.writeString(' index: ${tiles.length == 1 ? -1 : index}\n');
			}
		}
		return txt.getBytes();
	}
}
