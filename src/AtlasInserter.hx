import sys.io.File;

class AtlasInserter {
	public static function main() {
		final args = Sys.args();
		if (args.length != 2) {
			showUsage();
		}
		final output = args[0];
		final input = args[1];

		try {
			final atlas = File.getBytes(input);
			final png = openPng(output);
			final bytes = insertAtlas(png, atlas);
			File.saveBytes(output, bytes);
		} catch (e) {
			Sys.stderr().writeString('Could not insert atlas: ${e.message}\n');
			Sys.exit(-1);
		}
	}

	static function showUsage() {
		Sys.stderr().writeString("Usage: insertatlas file.png file.atlas\n");
		Sys.exit(-1);
	}

	static function openPng(name: String): format.png.Data {
		final png = new format.png.Reader(File.read(name)).read();
		return png.filter(isNotAtlasChunk);
	}

	static function isNotAtlasChunk(chunk: format.png.Data.Chunk): Bool {
		return switch(chunk) {
			case CUnknown("zTXt", data):
				final input = new haxe.io.BytesInput(data);
				final keyword = input.readUntil(0);
				keyword != 'Atlas';
			default:
				true;
		}
	}

	static function insertAtlas(png: format.png.Data, atlas: haxe.io.Bytes, ?level = 9 ): haxe.io.Bytes {
		final end = png.last();
		png.remove(end);
		final txt = new haxe.io.BytesOutput();
		txt.writeString('Atlas');
		txt.writeByte(0);
		txt.writeByte(0);
		txt.write(std.format.tools.Deflate.run(atlas, level));
		png.add(std.format.png.Data.Chunk.CUnknown('zTXt', txt.getBytes()));
		png.add(end);
		var o = new haxe.io.BytesOutput();
		new format.png.Writer(o).write(png);
		return o.getBytes();
	}
}
